# -*- coding:utf-8 -*-

import argparse
import math
import pybullet as p
from time import sleep

dt = 0.01

def init():
    """Initialise le simulateur
    
    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot
    
    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)
    
    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation
    
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

def cart_to_leg(x,z):
    L1,L2,L3,L4=40,45,65,87
    val=(-L4**2+z**2+x**2+L3**2)/(2*L3)
    theta1=2*math.atan2(z+math.sqrt(x**2+z**2-val**2),x+val)
    if(z>L3*math.sin(theta1)):
        theta2=theta1-math.acos((x-L3*math.cos(theta1))/L4)
    else:
        theta2=theta1+math.acos((x-L3*math.cos(theta1))/L4)
    return theta1,theta2

def leg_ik(x,y,z):
    angles = [0]*3
    theta0= math.atan2(y,x)
    l=x/math.cos(theta0)
    if(22>math.sqrt(x**2+y**2+z**2) or math.sqrt(x**2+y**2+z**2)>151):
        return joints
    theta1,theta2= cart_to_leg(l,z)
    angles[0:3] = theta0, theta1, theta2
    return angles

def move_leg(i, angles, joints):
    joints[i*3:i*3+3] = angles[0:3]

def robot_ik(x,y,z):
    joints = [0]*12
    for i in range(4):
        X,Y=robot_to_leg(x,y,i)
        X -= 50
        move_leg(i, leg_ik(-X,-Y,-z), joints)
    return joints

def robot_to_leg(x,y,leg):
    X,Y=[-1,-1,1,1],[1,-1,-1,1]
    theta=math.cos(math.pi/4)
    return (x*theta*X[leg]+y*theta*Y[leg], x*theta*X[(leg+1)%4]+y*theta*Y[(leg+1)%4])

def interpolate(xs,ts,t):
    if(t<ts[0] or t>ts[-1]):
        return 0
    i=dicho(ts,t,[0,len(ts)])
    if(i==len(ts)-1):
        return xs[-1]
    m=(xs[i+1]-xs[i])*1.0/(ts[i+1]-ts[i])
    return xs[i]+m*(t-ts[i])

def dicho(ts,t,tab):
    if (len(ts)<=1):
       return tab[0]
    if(ts[len(ts)//2]>t):
        return dicho(ts[:len(ts)//2],t,[tab[0],tab[0]+len(ts)//2])
    return dicho(ts[len(ts)//2:],t,[tab[0]+len(ts)//2,tab[1]])


def walk(xs,ys,rs,t):
    joints=[0]*12
    if(ys!=0 or xs!=0 or rs!=0):
        cycle=1.0
        t=t%cycle
        T=[0,cycle/8,cycle/4,cycle]
        Z=[-50,20,-50,-50]
        m=cycle*2000*3/2
        X=[-m*xs,0,m*xs,-m*xs]
        Y=[-m*ys,0,m*ys,-m*ys]
        for i in range(4):
            Dt=cycle/4*[2,0,3,1][i]
            x,y,z=interpolate(X,T,(t+Dt)%cycle),interpolate(Y,T,(t+Dt)%cycle),interpolate(Z,T,(t+Dt)%cycle)
            x,y=robot_to_leg(x,y,i)
            x+=50
            move_leg(i, leg_ik(x,y,z), joints)
    return joints

def goto(x, y, t):
    xsens = abs(x) / x
    speeds = [xsens*0.005,y*0.005/abs(x),t]
    xs = speeds [0]
    if xs*t == x or xs*t > x:
        return [0, 0, t]
    else:
        return speeds

if (__name__ == "__main__"):
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t = args.m, args.x, args.y, args.t
    
    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode == 'leg_ik':
        x = p.addUserDebugParameter("x", 20, 200, 100)
        y = p.addUserDebugParameter("y", -200, 200, 0)
        z = p.addUserDebugParameter("z", -100, 100, 0)
        print('Mode de positionnement dune patte')
    elif mode=='robot_ik':
        x = p.addUserDebugParameter("x", -100, 100, 0)
        y = p.addUserDebugParameter("y", -100, 100, 0)
        z = p.addUserDebugParameter("z", 0, 100, 80)
        print('Mode de positionnement du corps')
    elif mode == 'walk':
        xs = p.addUserDebugParameter("x speed", -0.1, 0.1,0)
        ys = p.addUserDebugParameter("y speed", -0.1, 0.1, 0)
        rs = p.addUserDebugParameter("rotation speed", -1, 1, 0)
        print('Mode de mouvement du robot...')
    elif mode == 'goto':
        print('Mode déplacement robot aux données')
    else:
        raise Exception('Mode non implémenté: %s' % mode)
    
    t = 0

    # Boucle principale
    while True:
        t += dt
        
        if mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))
        if mode == 'leg_ik':
            joints= [0]*12
            angles = leg_ik(p.readUserDebugParameter(x), p.readUserDebugParameter(y), p.readUserDebugParameter(z))
            move_leg(1, angles, joints)
        if mode == 'robot_ik':
            joints = robot_ik(p.readUserDebugParameter(x), p.readUserDebugParameter(y), p.readUserDebugParameter(z))
	if mode == 'walk':
            joints=walk(p.readUserDebugParameter(xs),p.readUserDebugParameter(ys),p.readUserDebugParameter(rs),t)
        if mode == 'goto':
            speeds = goto(x,y,t)
            joints=walk(speeds[0], speeds[1], speeds[2], t)
        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)